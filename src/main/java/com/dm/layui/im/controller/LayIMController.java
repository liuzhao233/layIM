package com.dm.layui.im.controller;

import com.dm.layui.im.constant.SocketConstant;
import com.dm.layui.im.entity.*;
import com.dm.layui.im.service.*;
import com.dm.layui.im.util.LayimUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author LiuZhao
 * @Date 2020/4/8 9:46
 * @Version 1.0
 */
@Controller
@RequestMapping(value = "layim")
public class LayIMController {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private FriendsService friendsService;
    @Autowired
    private MineService mineService;
    @Autowired
    private GroupsService groupsService;
    @Autowired
    private ChatMsgService chatMsgService;
    @Autowired
    private SysMsgService sysMsgService;

    /**
     * 通过用户id进入个人界面
     * @param userId
     * @param session
     * @return
     */
    @RequestMapping(value = "/{userId}",method = RequestMethod.GET)
    public String layim(@PathVariable("userId")String userId, HttpSession session){
        session.setAttribute("userId",userId);
        return "/index";
    }

    @GetMapping("/chat/log")
    public String toChatLog(){
        return "/chatlog";
    }

    @GetMapping("/add/find")
    public String addFind(){
        return "/find";
    }

    @GetMapping("/add/ask")
    public String addAsk(){
        return "/msgbox";
    }

    /**
     * 查询聊天记录
     * @param uid
     * @param session
     * @return
     * @throws InterruptedException
     */
    @GetMapping("/chat/log/{uid}")
    @ResponseBody
    public List<Mine> chatlog(@PathVariable("uid")String uid,HttpSession session) throws InterruptedException {
        String userid=(String) session.getAttribute("userId");
        Thread.sleep(1000);//模拟消息查询缓慢，让前台展示loading样式
        ChatMsg chatmsg=new ChatMsg().setSendUserId(userid).setReciveUserId(uid);
        List<Mine> mines = chatMsgService.getChatMsgLog(chatmsg);
        return mines;
    }

    /**
     * 查询群聊天记录
     * @param gid
     * @return
     * @throws InterruptedException
     */
    @GetMapping("/chat/log/group/{gid}")
    @ResponseBody
    public List<Mine> chatlog(@PathVariable("gid")String gid) throws InterruptedException {
        Thread.sleep(1000);//模拟消息查询缓慢，让前台展示loading样式
        List<Mine> mines = groupsService.getGroupChatLogMsg(new GroupMsg().setGroupId(gid));
        return mines;
    }

    @GetMapping("/add/ask/{uid}")
    @ResponseBody
    public List<Map<String,Object>> addAskRecord(@PathVariable("uid")String uid,HttpSession session) throws InterruptedException {
        String userid=(String) session.getAttribute("userId");
        Thread.sleep(1000);//模拟消息查询缓慢，让前台展示loading样式

        List<Map<String,Object>> layimAsks=new ArrayList<>();
        //从redis中取离线接收的消息
        String prefix=userid+"_"+ SocketConstant.ADD_ASK +"*";
        // 获取所有的key
        Set<String> keys = redisTemplate.keys(prefix);

        if(keys.size()!=0) {
            //遍历key
            for (String str : keys) {
                //获取消息数据
                Map<String,Object> addAsk = redisTemplate.opsForHash().entries(str);
                String read = addAsk.get("read").toString();
                if(read.equals("0")){
                    //设为已读
                    redisTemplate.opsForHash().put(str,"read","1");
                    //添加成功的消息
                    if(addAsk.size()==1){
                        redisTemplate.delete(str);
                    }
                }
                //处理返回数据
//                LayimAsk layimAsk=new LayimAsk().setId(addAsk.get("id").toString()).setContent(addAsk.get("content").toString())
                if(addAsk.size()!=1) {
                    layimAsks.add(addAsk);
                }
            }
        }
        //查系统消息
        List<SysMsg> sysMsgs = sysMsgService.getSysMsgByUid(userid);
        for(SysMsg sysMsg:sysMsgs){
            LayimAsk layimAsk=new LayimAsk().setId(sysMsg.getId()).setContent(sysMsg.getContent()).setUid(userid).setType("1")
                    .setTime(String.valueOf(sysMsg.getCreateTime().getTime())).setUser(new Mine().setId(""));
            Map<String, Object> map = LayimUtil.beanToMap(layimAsk);
            layimAsks.add(map);
        }

      return layimAsks;
    }

    @PostMapping("/add/agree")
    @ResponseBody
    public Mine addAskagree(@RequestParam String id) {
        //从redis中取离线接收的消息
        String prefix = "*" + id + "*";
        // 获取所有的key
        Set<String> keys = redisTemplate.keys(prefix);
        if (keys.size() != 0) {
            //遍历key
            for (String str : keys) {
                //获取消息数据
                Map<String, Object> addAsk = redisTemplate.opsForHash().entries(str);
                String uid = addAsk.get("uid").toString();
                String fid = addAsk.get("from").toString();
                String type=addAsk.get("type").toString();
                if(type.equals("0")){
                    //添加好友
                    boolean addFriend = friendsService.addFriend(new Friend().setUid(uid).setFid(fid));
                    if (addFriend) {
                        //删除redis
                        redisTemplate.delete(str);
                        //发送socket给发起人一个通知并把好友添加到列表
                        try {
                            Mine userInfo = mineService.getUserInfo(uid);
                            ChatWebSocket.addFriend(fid, userInfo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return (Mine) addAsk.get("user");
                    }
                }else if(type.equals("1")){
                    //添加群
//                    groupsService.addGroupUser(new Group().setUid(fid).setGid())
                }

            }
        }
        return null;
    }

    @PostMapping("/add/refuse")
    @ResponseBody
    public boolean addAskRefuse(@RequestParam String id) {
        //从redis中取离线接收的消息
        String prefix = "*" + id + "*";
        // 获取所有的key
        Set<String> keys = redisTemplate.keys(prefix);
        if (keys.size() != 0) {
            //遍历key
            for (String str : keys) {
                //获取消息数据
                Map<String, Object> addAsk = redisTemplate.opsForHash().entries(str);
                String uid = addAsk.get("uid").toString();
                String fid = addAsk.get("from").toString();
                boolean sysMsg = sysMsgService.addSysMsg(uid, fid);
                if(sysMsg){
                    //删除redis
                    redisTemplate.delete(str);
                    //发送socket给发起人一个通知并把好友添加到列表
                    try {
                        ChatWebSocket.addFriend(fid, null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 初始化layim
     * @param session
     * @return
     */
    @RequestMapping(value = "/init",method = RequestMethod.GET)
    @ResponseBody
    public InitImVo init(HttpSession session){
        String userId = (String) session.getAttribute("userId");
        InitImVo initImVo=new InitImVo();
        //个人信息
        Mine mine=mineService.getUserInfo(userId);
        //好友列表
        List<Mine> mineList=friendsService.getUserFriend(userId);
        Friends friends=new Friends().setId("2").setGroupname("我的好友").setList(mineList);
        List<Friends> friendList=new ArrayList<>();
        friendList.add(friends);
        //群组信息
        List<Groups> groupsList = groupsService.getUserGroups(userId);
        //Data数据
        ImData imData=new ImData();
        imData.setMine(mine);
        imData.setFriend(friendList);
        imData.setGroup(groupsList);
        initImVo.setCode(0);
        initImVo.setMsg("");
        initImVo.setData(imData);
        return initImVo;
    }

}
