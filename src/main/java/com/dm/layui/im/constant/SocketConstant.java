package com.dm.layui.im.constant;

/**
 * @Author LiuZhao
 * @Date 2020/4/10 14:41
 * @Version 1.0
 */
public class SocketConstant {
    //发消息的key
    public final static String ON_LINE_MESSAGE="onLineMsg";
    //添加请求
    public final static String ADD_ASK="addAsk";
    //消息盒子未读消息
    public final static String MSG_BOX_UNREAD="Unread";

}
