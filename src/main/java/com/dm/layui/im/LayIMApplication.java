package com.dm.layui.im;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan({"com.dm.layui.im.dao"})
@SpringBootApplication
public class LayIMApplication {
    public static void main(String[] args) {
        SpringApplication.run(LayIMApplication.class,args);
    }
}
