package com.dm.layui.im.dao;

import com.dm.layui.im.entity.Mine;
import java.util.List;

public interface MineMapper {

    /**
     * 更新用户信息
     * @param mine
     */
    boolean upUserMine(Mine mine);


    /**
     * 查询用户的信息
     * @param userId
     * @return
     */
    Mine getUserInfo(String userId);

    /**
     * 获取全部用户
     * @return
     */
    List<Mine> getMineList();
}
