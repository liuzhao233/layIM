package com.dm.layui.im.dao;

import com.dm.layui.im.entity.Friend;
import com.dm.layui.im.entity.Groups;
import com.dm.layui.im.entity.Mine;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author LiuZhao
 * @Date 2020/4/8 8:39
 * @Version 1.0
 */
public interface FriendsMapper {

    /**
     * 添加好友
     * @param friend
     * @return
     */
    boolean addFriend(Friend friend);

    /**
     * 查询用户的好友列表
     * @param userId
     * @return
     */
    List<Mine> getUserFriend(@Param("userId") String userId);


}
