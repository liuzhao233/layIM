package com.dm.layui.im.dao;

import com.dm.layui.im.entity.SysMsg;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface SysMsgMapper {

    /**
     * 添加系统消息
     * @param sysMsg
     */
    boolean addSysMsg(SysMsg sysMsg);

    /**
     * 根据用户id获取系统消息
     * @param uid
     * @return
     */
    List<SysMsg> getSysMsgByUid(@Param("uid") String uid);

}
