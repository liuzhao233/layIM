package com.dm.layui.im.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
/**
 * @Author LiuZhao
 * @Date 2020/4/7 20:26
 * @Version 1.0
 */
//@Configuration
public class MvcConfig extends WebMvcConfigurationSupport {

    // 配置静态资源(配置这个后默认配置将不再生效,也就是WebMvcAutoConfiguration(自动配置静态资源类)类将不在生效)
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/",
            "classpath:/resources/", "classpath:/static/", "classpath:/public/",
            "classpath:webapp/", "/" };

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String[] staticLocations = CLASSPATH_RESOURCE_LOCATIONS;
        ResourceHandlerRegistration resourceHandlerRegistration = registry
                .addResourceHandler("/**");
        for (String staticLocation : staticLocations) {
            resourceHandlerRegistration.addResourceLocations(staticLocation);
        }
        super.addResourceHandlers(registry);
    }

    /**
     * 配置项目首页
     * @param registry
     */
//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        // 配置默认访问的controller
//        // registry.addRedirectViewController("/", "student");
//        // 配置默认访问的静态页面(默认就找index.html)
//        ViewControllerRegistration addViewController = registry.addViewController("/");
//        addViewController.setViewName("forward:index.html");
//        // 设置优先级
//        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
//        super.addViewControllers(registry);
//    }


}
