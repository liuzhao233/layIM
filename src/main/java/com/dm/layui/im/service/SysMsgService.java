package com.dm.layui.im.service;


import com.dm.layui.im.entity.SysMsg;

import java.util.List;

/**
 * @Author LiuZhao
 * @Date 2020/4/8 15:13
 * @Version 1.0
 */
public interface SysMsgService {
    /**
     * 添加系统消息
     * @param uid
     * @param fid
     * @return
     */
    boolean addSysMsg(String uid,String fid );

    /**
     * 根据用户id获取系统消息
     * @param uid
     * @return
     */
    List<SysMsg> getSysMsgByUid( String uid);
}
