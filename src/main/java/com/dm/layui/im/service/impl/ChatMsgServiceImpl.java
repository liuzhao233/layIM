package com.dm.layui.im.service.impl;

import com.dm.layui.im.dao.ChatMsgMapper;
import com.dm.layui.im.entity.ChatMsg;
import com.dm.layui.im.entity.Mine;
import com.dm.layui.im.service.ChatMsgService;
import com.dm.layui.im.util.IdGenerat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author LiuZhao
 * @Date 2020/4/8 15:14
 * @Version 1.0
 */
@Service
public class ChatMsgServiceImpl implements ChatMsgService {
    @Autowired
    private ChatMsgMapper chatMsgMapper;

    @Override
    public boolean insertChatmsg(ChatMsg chatMsg) {
        chatMsg.setId(IdGenerat.getGeneratID());
//        chatMsg.setCreateTime(new Date());
        return chatMsgMapper.insertChatmsg(chatMsg);
    }

    @Override
    public List<Mine> getChatMsgLog(ChatMsg chatMsg) {
        return chatMsgMapper.getChatMsgLog(chatMsg);
    }
}
