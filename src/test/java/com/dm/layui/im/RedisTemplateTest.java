package com.dm.layui.im;

import com.dm.layui.im.entity.ChatMsg;
import com.dm.layui.im.entity.LayimAsk;
import com.dm.layui.im.entity.Mine;
import com.dm.layui.im.util.LayimUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * @Author LiuZhao
 * @Date 2020/4/9 14:02
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTemplateTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test(){
//        Map<String,Object> map=new HashMap<>();
//        String time=String.valueOf(new Date().getTime());
//        String Key="1571476959767947441"+time;
//        map.put("receiveId","1571476959767947441");
//        map.put("sendId","1571476959767947449");
//        map.put("content","reids离线发送");
//        map.put("sendTime",new Date().getTime());
//        redisTemplate.opsForHash().putAll(Key,map);
//
//        Map<String,Object> map2 = redisTemplate.opsForHash().entries("1571476959767947441_1571476959767947441");
//        System.out.println(map2);
//        redisTemplate.opsForValue().set("aaa","bbb");
//        redisTemplate.opsForHash().
        String prefix="1571476959767947441"+"*";// 获取所有的key
        Set<String> keys = redisTemplate.keys(prefix);
        System.out.println(keys);
        for(String str:keys){
            Map<String,Object> map2 = redisTemplate.opsForHash().entries(str);
            System.out.println(map2);
            redisTemplate.delete(str);
        }
//        System.out.println(redisTemplate.opsForHash().multiGet());
    }

    @Test
    public void groupMsg(){
        String time=String.valueOf(new Date().getTime());
        String groupId="1585282806795518697";
//        String hashKey=groupId+"_1571476959767947427_"+time;

        List<String> userIds=new ArrayList<>();
        userIds.add("1571476959767947441");
        userIds.add("1571476959767947449");

        Map<String,Object> map=new HashMap<>();
        map.put("receiveId","1585282806795518697");
        map.put("sendId","1571476959767947427");
        map.put("content","群发");
        map.put("sendTime",new Date().getTime());

        for(String str:userIds){
            String hashKey=str+"_"+time+"_group";
            redisTemplate.opsForHash().put(hashKey,str,"¿¿¿¿¿¿¿¿¿¿");
        }



    }

    @Test
    public void addAsk(){
        String uid="1571476959767947441";
        String time=String.valueOf(new Date().getTime());
        Mine mine=new Mine().setId("1571476959767947448").setUsername("我是小E同学").setAvatar("http://q.qlogo.cn/qqapp/101235792/B704597964F9BD0DB648292D1B09F7E8/100")
                .setSign("");

        LayimAsk layimAsk=new LayimAsk().setId(IdGenerat.getGeneratID()).setUid(uid)
                .setFrom("1571476959767947448").setFromGroup("0").setContent("申请添加你为好友")
                .setRemark("我是小E同学").setTime(time).setHref("")
                .setRead("0").setType("1").setUser(mine);
        Map<String, Object> map = LayimUtil.beanToMap(layimAsk);

        String key=uid+"_addAsk_"+time+"_friend";

        redisTemplate.opsForHash().putAll(key,map);


        String prefix="1571476959767947441"+"_addAsk*";// 获取所有的key
        Set<String> keys = redisTemplate.keys(prefix);
        System.out.println(keys);
        for(String str:keys){
            Map<String,Object> map2 = redisTemplate.opsForHash().entries(str);
            System.out.println(map2);
//            redisTemplate.delete(str);
        }
    }

}
