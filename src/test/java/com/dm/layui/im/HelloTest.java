package com.dm.layui.im;

import com.alibaba.fastjson.JSONObject;
import com.dm.layui.im.controller.ChatWebSocket;
import com.dm.layui.im.dao.*;
import com.dm.layui.im.entity.ChatMsg;
import com.dm.layui.im.entity.Groups;
import com.dm.layui.im.entity.Mine;
import com.dm.layui.im.entity.SysMsg;
import com.dm.layui.im.service.ChatMsgService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloTest {

    @Autowired
    private MineMapper mineMapper;
    @Autowired
    private FriendsMapper friendsMapper;
    @Autowired
    private GroupsMapper groupsMapper;
    @Autowired
    private ChatMsgService chatMsgService;
    @Autowired
    private SysMsgMapper sysMsgMapper;

    @Test
    public void test(){
        System.out.println("test");
    }

    @Test
    public void getMineList(){
        List<Mine> mineList = mineMapper.getMineList();
        for(Mine mine:mineList){
            System.out.println(mine.toString());
        }
    }

    @Test
    public void getUserFriend(){
        List<Mine> userFriend = friendsMapper.getUserFriend("1571476959767947441");
        for(Mine mine:userFriend){
            System.out.println(mine.toString());
        }
    }

    @Test
    public void getUserGroups(){
        List<Groups> userGroups = groupsMapper.getUserGroups("1571476959767947441");
        for(Groups groups:userGroups){
            System.out.println(groups.toString());
        }
    }

    @Test
    public void getGroupUserById(){
        List<Mine> userGroups = groupsMapper.getGroupUserById("1585282806795518697");
        for(Mine groups:userGroups){
            System.out.println(groups.toString());
        }
    }

    @Test
    public void sendMsg(){
        ChatMsg chatMsg=new ChatMsg().setId(IdGenerat.getGeneratID()).setSendUserId("1571476959767947441").setReciveUserId("1571476959767947449")
                .setContent("bababba").setMsgType("0").setCreateTime(new Date());
        boolean insertChatmsg = chatMsgService.insertChatmsg(chatMsg);
        if(insertChatmsg){
            System.out.println("success");
        }else{
            System.out.println("fail");
        }
    }

    @Test
    public void sendToUser() {
        Mine mine=new Mine().setId("1571476959767947448").setUsername("小E同学").setAvatar("/pic/xd.png").setToid("1571476959767947441")
                .setContent("我是小E同学").setType("0").setSendtime(new Date());
        ChatWebSocket.sendToUser(mine);
    }

    @Test
    public void addSysMsg() {
        SysMsg sysMsg=new SysMsg().setId(IdGenerat.getGeneratID()).setContent("hahahahahah")
                .setUid("1571476959767947441").setCreateTime(new Date()).setStatus("0");
        sysMsgMapper.addSysMsg(sysMsg);
    }
}
