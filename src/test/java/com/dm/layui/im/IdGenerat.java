package com.dm.layui.im;

import com.dm.layui.im.dao.FriendsMapper;
import com.dm.layui.im.dao.GroupsMapper;
import com.dm.layui.im.dao.MineMapper;
import com.dm.layui.im.entity.ChatMsg;
import com.dm.layui.im.entity.Groups;
import com.dm.layui.im.entity.Mine;
import com.dm.layui.im.service.ChatMsgService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IdGenerat {
    public static String getDate(String sformat) {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(sformat);
        String dateString = formatter.format(currentTime);
        return dateString;
     }

    public static String getRandomNum(int num){
         String numStr = "";
         for(int i = 0; i < num; i++){
             numStr += (int)(10*(Math.random()));
         }
         return numStr;
     }

    public static String getGeneratID(){
        long time = new Date().getTime();
        String idStr =String.valueOf(time)+getRandomNum(6);
        return idStr;
     }

     @Test
    public void idGenerat(){
        for(int i=0;i<100;i++){
            System.out.println(getGeneratID());
        }
     }
}
